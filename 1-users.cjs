const users = {
    "John": {
        age: 24,
        desgination: "Senior Golang Developer",
        interests: ["Chess, Reading Comics, Playing Video Games"],
        qualification: "Masters",
        nationality: "Greenland"
    },
    "Ron": {
        age: 19,
        desgination: "Intern - Golang",
        interests: ["Video Games"],
        qualification: "Bachelor",
        nationality: "UK"
    },
    "Wanda": {
        age: 24,
        desgination: "Intern - Javascript",
        interests: ["Piano"],
        qualification: "Bachaelor",
        nationality: "Germany"
    },
    "Rob": {
        age: 34,
        desgination: "Senior Javascript Developer",
        interest: ["Walking his dog, Cooking"],
        qualification: "Masters",
        nationality: "USA"
    },
    "Pike": {
        age: 23,
        desgination: "Python Developer",
        interests: ["Listing Songs, Watching Movies"],
        qualification: "Bachaelor's Degree",
        nationality: "Germany"
    }
}


/*

Q1 Find all users who are interested in playing video games.
Q2 Find all users staying in Germany.
Q3 Sort users based on their seniority level 
   for Designation - Senior Developer > Developer > Intern
   for Age - 20 > 10
Q4 Find all users with masters Degree.
Q5 Group users based on their Programming language mentioned in their designation.

NOTE: Do not change the name of this file

*/ 


// Q1 Find all users who are interested in playing video games.
usersArray=Object.entries(users)
const usersPlayingVideoGames=usersArray.filter((user) => {
    if(user[1].interests){
        interests=user[1].interests.toString().replace(/\s*,\s*/g, ",").split(",")
    return interests.includes("Playing Video Games") 
    }else {
        interest=user[1].interest.toString().replace(/\s*,\s*/g, ",").split(",")
    return interest.includes("Playing Video Games") 
    }
})
console.log(usersPlayingVideoGames)

// Q2 Find all users staying in Germany.

usersArray=Object.entries(users)
const usersInGermany=usersArray.filter((user) => {
    if(user[1].nationality){
        nationality=user[1].nationality
    return nationality.includes("Germany") 
    }
})
console.log(usersInGermany)

// Q3 Sort users based on their seniority level 
// for Designation - Senior Developer > Developer > Intern
// for Age - 20 > 10


usersArray=Object.entries(users)
const sortedBySeniority=usersArray.map((user) => {
    if(user[1].desgination.includes("Senior")){
        user[1]["level"]=3
    }
    else if(user[1].desgination.includes("Developer")){
        user[1]["level"]=2
    }
    else if(user[1].desgination.includes("Intern")){
        user[1]["level"]=1
    }
    return user[1]
}).sort((currentUser, nextUser) => {
    if(currentUser.level === nextUser.level){
        return  nextUser.age - currentUser.age 
    }
    else{
        return  nextUser.level - currentUser.level 
    }
})
console.log(sortedBySeniority)



// Q4 Find all users with masters Degree.
usersArray=Object.entries(users)
const usersWithMasters=usersArray.filter((user) => {
    if(user[1].qualification){
        qualification=user[1].qualification
    return qualification.includes("Masters") 
    }
})
console.log(usersWithMasters)

// Q5 Group users based on their Programming language mentioned in their designation.

usersArray=Object.entries(users)
const groupByLanguage=usersArray.reduce((groupUsers,user) => {
    if(user[1].desgination.includes("Python")){
        if(groupUsers["Python"]){
            user[1]["name"]=user[0]
            groupUsers("Python").push(user[1])
        }
        else{
            groupUsers["Python"]=[]
            user[1]["name"]=user[0]
            groupUsers["Python"].push(user[1])
        }
    }
    else if(user[1].desgination.includes("Javascript")){
        if(groupUsers["Javascript"]){
            user[1]["name"]=user[0]
            groupUsers["Javascript"].push(user[1])
        }
        else{
            groupUsers["Javascript"]=[]
            user[1]["name"]=user[0]
            groupUsers["Javascript"].push(user[1])
        }
    }
    else if(user[1].desgination.includes("Golang")){
        if(groupUsers["Golang"]){
            user[1]["name"]=user[0]
            groupUsers["Golang"].push(user[1])
        }
        else{
            groupUsers["Golang"]=[]
            user[1]["name"]=user[0]
            groupUsers["Golang"].push(user[1])
        }
    }
    return groupUsers
},{})
console.log(groupByLanguage)
